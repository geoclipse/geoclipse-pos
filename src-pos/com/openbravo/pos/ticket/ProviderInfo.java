//    geoclipse oPOS  - Touch Friendly Point Of Sale
//    Copyright (c) 2009-2012 geoclipse
//    http://www.geoclipse.net/geoclipseopos
//
//    This file is part of geoclipse oPOS
//
//    geoclipse oPOS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//   geoclipse oPOS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with geoclipse oPOS.  If not, see <http://www.gnu.org/licenses/>.

package com.openbravo.pos.ticket;

public class ProviderInfo {
    
    private int m_iProviderID;
    private String m_sName;

    /** Creates new Provider */
    public ProviderInfo() {
        m_iProviderID = 0;
        m_sName = "";
    }
    
    public int getProviderID() {
        return m_iProviderID;
    }
    
    public void setProviderID(int iProviderID) {
        m_iProviderID = iProviderID;
    }
    
    public String getName() {
        return m_sName;
    }
    
    public void setName(String sName) {
        m_sName = sName;
    }
    
    public String toString(){
        return m_sName;
    }
}
