//    geoclipse oPOS  - Touch Friendly Point Of Sale
//    Copyright (c) 2009-2012 geoclipse
//    http://www.geoclipse.net/geoclipseopos
//
//    This file is part of geoclipse oPOS
//
//    geoclipse oPOS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//   geoclipse oPOS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with geoclipse oPOS.  If not, see <http://www.gnu.org/licenses/>.

package com.openbravo.pos.ticket;

import com.openbravo.format.Formats;


public class TicketTaxInfo {
    
    private TaxInfo tax;
    private double subtotal;
    private double taxtotal;
    
    
            
    /** Creates a new instance of TicketTaxInfo */
    public TicketTaxInfo(TaxInfo tax  ) {
        this.tax = tax;
          
        subtotal = 0.0;
        taxtotal = 0.0;
    }
    public void setSubTotal(double subtotal){
        this.subtotal = subtotal;
    }
    
    public void setTaxTotal(double taxtotal){
        this.taxtotal = taxtotal;
    }
    public TaxInfo getTaxInfo() {
        return tax;
    }
    
    public void add(double dValue) {
        
        subtotal += dValue;
        taxtotal = subtotal * tax.getRate();
    }
    
    public double getSubTotal() {    
        return subtotal;
    }
    
    public double getTax() {
         
        return taxtotal;
    }
    
    public double getTotal() {         
        return subtotal + taxtotal;
    }
    
    public String printSubTotal() {
        return Formats.CURRENCY.formatValue(new Double(getSubTotal()));
    }
    public String printTax() {
        return Formats.CURRENCY.formatValue(new Double(getTax()));
    }    
    public String printTotal() {
        return Formats.CURRENCY.formatValue(new Double(getTotal()));
    }    
    
}
